package de.xantoxx.tictactoeplugin.commands;

import de.xantoxx.tictactoeplugin.logic.TicTacToeGame;
import de.xantoxx.tictactoeplugin.logic.TicTacToeRequest;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TicTacToeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player1 = (Player) sender;

            if (args.length == 1) {
                Player player2 = Bukkit.getPlayer(args[0]);
                if (player2 != null) {
                    if (player1 != player2) {
                        TicTacToeRequest request = new TicTacToeRequest(player1, player2);
                        // Schaut, ob der, der den Command eingegeben hat schon eingeladen wurde
                        // und ob der, der ihn eingeladen hat auch der ist, den man gerade einlädt
                        for (TicTacToeRequest currentRequest : TicTacToeRequest.ticTacToeRequests) {
                            if (currentRequest.equals(request)) {
                                player1.sendMessage("§cDu hast " + player2.getName() + " bereits eingeladen!");
                                return true;
                            }
                            if (player1 == currentRequest.getReceiver()) {
                                if (player2 == currentRequest.getSender()) {
                                    start(player2, player1, currentRequest);
                                    return true;
                                }
                            }
                        }
                        invite(player1, player2, request);
                    } else
                        player1.sendMessage("§cDu kannst nicht mit dir selbst spielen!");
                } else
                    player1.sendMessage("§c" + args[0] + " wurde nicht gefunden");
            } else
                player1.sendMessage("§cGebe einen Spielernamen an!");
        }
        return false;
    }

    public void start(Player player1, Player player2, TicTacToeRequest request) {
        TicTacToeRequest.ticTacToeRequests.remove(request);
        new TicTacToeGame(player1, player2);
    }

    public void invite(Player sender, Player receiver, TicTacToeRequest request) {
        TicTacToeRequest.ticTacToeRequests.add(request);

        sender.sendMessage("§bDu hast " + receiver.getName() + " zu TicTacToe herausgefordert");

        receiver.sendMessage("§bDu wurdest von " + sender.getName() + " zu TicTacToe herausgefordert");
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tellraw " + receiver.getName() + " {\"text\":\"/tic " + sender.getName() +
                " oder hier clicken: §a[Annehmen]\",\"color\":\"aqua\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/tic " + sender.getName() + "\"}}");
    }

}
