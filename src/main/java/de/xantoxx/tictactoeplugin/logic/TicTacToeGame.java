package de.xantoxx.tictactoeplugin.logic;

import de.xantoxx.tictactoeplugin.TicTacToePlugin;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class TicTacToeGame implements Listener {
    private final Player player1;
    private final Player player2;
    private Player currentPlayer;
    private final Inventory inv;

    private final Material MATERIAL_PLAYER1 = Material.EMERALD;
    private final Material MATERIAL_PLAYER2 = Material.DIAMOND;
    private final Material MATERIAL_BACKGROUND = Material.GRAY_STAINED_GLASS_PANE;
    private final Material MATERIAL_GIVE_UP = Material.RED_STAINED_GLASS_PANE;

    public static List<Player> currentlyPlaying = new ArrayList<>();

    public TicTacToeGame(Player player1, Player player2) {
        TicTacToePlugin.registerEvent(this);

        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = this.player1;

        currentlyPlaying.add(player1);
        currentlyPlaying.add(player2);

        Inventory inventory = Bukkit.createInventory(null, InventoryType.WORKBENCH, "Tic Tac Toe");
        this.inv = initializeInventory(inventory);

        player1.openInventory(this.inv);
        player2.openInventory(this.inv);
    }

    @EventHandler
    public void clickListener(InventoryClickEvent event) {
        if (event.getInventory().equals(this.inv)) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null) {
                Player clicker = (Player) event.getWhoClicked();
                if (event.getCurrentItem().getType().equals(MATERIAL_BACKGROUND)) {
                    if (clicker.equals(currentPlayer)) {
                        event.getCurrentItem().setType(colorOf(currentPlayer));

                        if (checkIfCurrentPlayerWon()) {
                            endGame();
                            currentPlayer.sendTitle("", "§aDu hast Gewonnen!", 10, 30, 10);
                            getOtherPlayer(currentPlayer).sendTitle("", "§cLeider verloren", 10, 30, 10);
                        } else if (checkIfTie()) {
                            endGame();
                            player1.sendTitle("", "§eUnentschieden", 10, 30, 10);
                            player2.sendTitle("", "§eUnentschieden", 10, 30, 10);
                        } else
                            nextPlayer();
                    }
                } else if (event.getCurrentItem().getType().equals(MATERIAL_GIVE_UP)) {
                    cancelGame(clicker, getOtherPlayer(clicker));
                }
            }
        }
    }

    public void cancelGame(Player droupout, Player winner) {
        endGame();
        droupout.sendTitle("", "§cDu hast aufgegeben", 10, 30, 10);
        winner.sendTitle("", "§a" + droupout.getName() + " hat aufgegeben", 10, 30, 10);
    }

    public void endGame() {
        currentlyPlaying.remove(player1);
        currentlyPlaying.remove(player2);
        player1.closeInventory();
        player2.closeInventory();
    }

    public boolean checkIfCurrentPlayerWon() {
        // Schaut ob gerade nach unten alle Spielsteine gleich sind
        for (int i = 1; i < 4; i++)
            if (equals(inv.getItem(i), inv.getItem(i + 3), inv.getItem(i + 6)))
                return returnWinner(i);
        // Schaut ob von links nach rechts alle Spielsteine gleich sind
        for (int i = 1; i < 10; i += 3)
            if (equals(inv.getItem(i), inv.getItem(i + 1), inv.getItem(i + 2)))
                return returnWinner(i);

        // Schaut ob schräg alle Spielsteine gleich sind
        if (equals(inv.getItem(1), inv.getItem(5), inv.getItem(9)))
            return returnWinner(1);

        if (equals(inv.getItem(3), inv.getItem(5), inv.getItem(7)))
            return returnWinner(3);

        return false;
    }

    public boolean equals(ItemStack item1, ItemStack item2, ItemStack item3) {
        return item1.equals(item2) && item1.equals(item3);
    }

    public boolean returnWinner(int i) {
        if (inv.getItem(i).getType() != MATERIAL_BACKGROUND)
            return inv.getItem(i).getType().equals(colorOf(currentPlayer));
        return false;
    }

    public Player getOtherPlayer(Player player) {
        if (player.equals(player1))
            return player2;
        else
            return player1;
    }

    public boolean checkIfTie() {
        boolean tie = true;

        for (int i = 0; i < inv.getSize(); i++) {
            if (inv.getItem(i).getType() == this.MATERIAL_BACKGROUND)
                tie = false;
        }
        return tie;
    }

    public Material colorOf(Player player) {
        if (player.equals(player1)) {
            return this.MATERIAL_PLAYER1;
        } else
            return this.MATERIAL_PLAYER2;
    }

    public void nextPlayer() {
        if (this.currentPlayer.equals(player1)) {
            this.currentPlayer = player2;
        } else
            this.currentPlayer = player1;
    }

    public Inventory initializeInventory(Inventory inventory) {
        ItemStack item = new ItemStack(MATERIAL_BACKGROUND);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(" ");
        item.setItemMeta(itemMeta);

        ItemStack giveUp = new ItemStack(MATERIAL_GIVE_UP);
        ItemMeta giveUpMeta = item.getItemMeta();
        giveUpMeta.setDisplayName("§4Aufgeben");
        giveUp.setItemMeta(giveUpMeta);

        for (int i = 1; i < 10; i++) {
            inventory.setItem(i, item);
        }
        inventory.setItem(0, giveUp);
        return inventory;
    }
}
