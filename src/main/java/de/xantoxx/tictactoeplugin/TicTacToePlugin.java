package de.xantoxx.tictactoeplugin;

import de.xantoxx.tictactoeplugin.commands.TicTacToeCommand;
import de.xantoxx.tictactoeplugin.listener.CloseTicTacToeListener;
import de.xantoxx.tictactoeplugin.logic.TicTacToeGame;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/*
    Tic Tac Toe Plugin by Xantoxx

    Disclaimer: Das war mein erstes Plugin, wahrscheinlich gibt es vieles, das man besser machen kann.
 */

public final class TicTacToePlugin extends JavaPlugin {

    private static TicTacToePlugin plugin;

    @Override
    public void onEnable() {
        System.out.println("[TicTacToePlugin] started");
        System.out.println("[TicTacToePlugin] developed by Xantoxx");

        plugin = this;
        PluginManager pluginManager = Bukkit.getPluginManager();

        // Commands initialisieren
        getCommand("tic").setExecutor(new TicTacToeCommand());

        // Listener initialisieren
        pluginManager.registerEvents(new CloseTicTacToeListener(), this);
    }

    public static void registerEvent(TicTacToeGame ticTacToeGame) {
        Bukkit.getPluginManager().registerEvents(ticTacToeGame, getPlugin());
    }

    @Override
    public void onDisable() {
        System.out.println("[TicTacToePlugin] disabled");
    }

    public static TicTacToePlugin getPlugin() {
        return plugin;
    }

}
