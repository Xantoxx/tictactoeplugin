package de.xantoxx.tictactoeplugin.listener;

import de.xantoxx.tictactoeplugin.TicTacToePlugin;
import de.xantoxx.tictactoeplugin.logic.TicTacToeGame;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class CloseTicTacToeListener implements Listener {

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getView().getTitle().equals("Tic Tac Toe")) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(TicTacToePlugin.getPlugin(), () -> {
                if (TicTacToeGame.currentlyPlaying.contains(event.getPlayer())) {
                    event.getPlayer().openInventory(event.getInventory());
                }
            }, 0);
        }
    }

}
